import java.awt.Point;
import java.util.LinkedList;
import java.util.List;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class OceanExplorer extends Application {

    private final int NUM_ISLANDS = 10;
    private final int NUM_PIRATES = 2;
    private final int GRID_X_SIZE = 10;
    private final int GRID_Y_SIZE = 10;
    private final int WINDOW_WIDTH = 500;
    private final int WINDOW_HEIGHT = 500;
    private final int xScale;
    private final int yScale;
    private final String WINDOW_NAME = "A Modestly Sized Ocean";
    private final String SHIP_IMG_PATH = "file:ship.png";
    private final String PIRATE_IMG_PATH = "file:pirateShip.png";
    Scene scene;
    Pane root;
    ImageView shipImageView;
    Image shipImage;
    OceanMap oceanMap;
    Ship columbusShip;
    List<PirateShip> pirateShips;

    public OceanExplorer() {
        xScale = WINDOW_WIDTH / GRID_X_SIZE;
        yScale = WINDOW_HEIGHT / GRID_Y_SIZE;
        oceanMap = new OceanMap( GRID_X_SIZE, GRID_Y_SIZE, NUM_ISLANDS, NUM_PIRATES );
        columbusShip = new Ship( oceanMap );
        pirateShips = new LinkedList<>();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new AnchorPane();
        scene = new Scene( root, WINDOW_WIDTH, WINDOW_HEIGHT );
        primaryStage.setScene(scene);
        primaryStage.setTitle(WINDOW_NAME);
        drawMap();
        loadColumbusShip();
        loadPirates();
        primaryStage.show();
        startSailing();
    }

    /* Loads an image at the specified coordinates

     */
    private void loadImage(String imgPath, int x, int y) {
        shipImage = new Image(imgPath, xScale, yScale, true, true);
        shipImageView = new ImageView(shipImage);
        shipImageView.setX(x);
        shipImageView.setY(y);
        root.getChildren().add(shipImageView);
    }

    private void loadColumbusShip() {
        Point shipLoc = columbusShip.getPosition();
        loadImage(SHIP_IMG_PATH, shipLoc.x * xScale, shipLoc.y * yScale);
    }

    private void loadPirates() {
        List<Point> pirates = oceanMap.getPirateStartLocations();
        for( Point p : pirates ) {
            PirateShip pirate = new PirateShip(oceanMap, p.x, p.y);
            pirateShips.add(pirate);
            columbusShip.addObserver(pirate);
            loadImage(PIRATE_IMG_PATH, p.x * xScale, p.y * yScale);
        }
    }

    private void drawMap() {
        int[][] map = oceanMap.getOceanMap();
        for( int x = 0; x < GRID_X_SIZE; x++ ) {
            for( int y = 0; y < GRID_Y_SIZE; y++ ) {
                Rectangle rect = new Rectangle(x * xScale, y * yScale, xScale, yScale);
                rect.setStroke(Color.BLACK);
                if (map[x][y] == 1) {
                    rect.setFill(Color.GREEN);
                } else {
                    rect.setFill(Color.PALETURQUOISE);
                }
                root.getChildren().add(rect);
            }
        }
    }

    private void startSailing() {
        scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
        @Override
        public void handle(KeyEvent ke) { switch(ke.getCode()){
            case RIGHT: columbusShip.goEast();
                break; case LEFT:
                columbusShip.goWest();
                break; case UP:
                columbusShip.goNorth();
                break; case DOWN:
                columbusShip.goSouth();
                break; default:
                break; }
            shipImageView.setX(columbusShip.getPosition().x * xScale);
            shipImageView.setY(columbusShip.getPosition().y * yScale);
        }
    }); }

    public static void main(String[] args) {
        launch(args);
    }

}
