import java.awt.Point;
import java.util.Observable;

public class Ship extends Observable {
    private Point currentPosition;
    private OceanMap oceanMap;


    /* Constructor
       This class must be instantiated AFTER OceanMap
     */
    public Ship(OceanMap oceanMap) {
        this.oceanMap = oceanMap;
        currentPosition = oceanMap.getShipStartLocation();
    }

    public Point getPosition() {
        return currentPosition;
    }

    public void goNorth() {
        if (currentPosition.y > 0 && oceanMap.isOcean(currentPosition.x, currentPosition.y - 1) ) {
            currentPosition.y--;
            setChanged();
            notifyObservers();
        }

    }

    public void goEast() {
        if (currentPosition.x < (oceanMap.X_SIZE - 1) && oceanMap.isOcean((currentPosition.x + 1), currentPosition.y )) {
            currentPosition.x++;
            setChanged();
            notifyObservers();
        }
    }

    public void goSouth() {
        if (currentPosition.y < (oceanMap.Y_SIZE - 1) && oceanMap.isOcean(currentPosition.x, currentPosition.y + 1)) {
            currentPosition.y++;
            setChanged();
            notifyObservers();
        }
    }

    public void goWest() {
        if (currentPosition.x > 0 && oceanMap.isOcean((currentPosition.x - 1), currentPosition.y )) {
            currentPosition.x--;
            setChanged();
            notifyObservers();
        }
    }

}