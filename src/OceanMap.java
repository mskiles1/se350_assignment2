import java.awt.*;
import java.util.List;
import java.util.LinkedList;
import java.util.Random;

public class OceanMap {
    final int X_SIZE;
    final int Y_SIZE;
    final int ISLAND_COUNT;
    private final Point shipStartLocation;
    private List<Point> pirateStartLocations;
    private Random rand;
    private int[][] oceanMap; //0 is water, 1 is island, 2 is pirate

    public OceanMap(int xDim, int yDim, int numIslands, int numPirates) {
        rand = new Random();
        X_SIZE = xDim;
        Y_SIZE = yDim;
        ISLAND_COUNT = numIslands;
        oceanMap = new int[xDim][yDim];
        generateIslands();
        shipStartLocation = findOpenPoint();
        placePirateShips(numPirates);
    }

    public int[][] getOceanMap() {
        return oceanMap;
    }

    public Point getShipStartLocation() {
        return shipStartLocation;
    }

    public List<Point> getPirateStartLocations() { return pirateStartLocations; }

    public boolean isOcean( int x, int y ) {
        return (oceanMap[x][y] == 0);
    }

    private Point findOpenPoint() {
        int x;
        int y;
        do {
            x = rand.nextInt(X_SIZE);
            y = rand.nextInt(Y_SIZE);
        } while ( !isOcean(x, y) );
        Point open = new Point(x, y);
        return open;
    }

    private void placePirateShips(int numPirates) {
        pirateStartLocations = new LinkedList<>();
        for( int i = 0; i < numPirates; i++ ) {
            Point newPirate = findOpenPoint();
            oceanMap[newPirate.x][newPirate.y] = 2;
            pirateStartLocations.add(newPirate);
        }
    }

    private void generateIslands() {
        int islandCount = 0;
        while ( islandCount < ISLAND_COUNT ) {
            int x = rand.nextInt(X_SIZE);
            int y = rand.nextInt(Y_SIZE);
            if ( !isOcean(x, y) ) // something is already there
                continue;
            else {
                oceanMap[x][y] = 1;
                islandCount++;
            }
        }
    }

}